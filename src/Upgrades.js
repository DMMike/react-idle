import React from "react";
import {UPGRADES_DATA} from "./App";

export default class Upgrades extends React.Component {
    renderLine(identifier) {
        const cost = Math.ceil(UPGRADES_DATA[identifier].start * Math.pow(UPGRADES_DATA[identifier].inc, this.props.upgrades[identifier]));

        return (
            <div>
                <div className="button" onClick={() => this.props.upgradesHandler(identifier)}>
                    {UPGRADES_DATA[identifier].name} (Level {this.props.upgrades[identifier] + 1}) ({cost})
                </div>
            </div>
        );
    }

    render() {
        const upgradeIdentifiers = Object.keys(UPGRADES_DATA),
            list = upgradeIdentifiers.map(identifier => this.renderLine(identifier));

        return (
            <div>{list}</div>
        );
    }
}