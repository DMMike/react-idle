import './App.css';
import React from "react";
import Upgrades from "./Upgrades";

export const UPGRADES_DATA = {
    auto: {
        start: 50,
        inc: 1.7,
        name: 'Automatic Income'
    },
    click: {
        start: 10,
        inc: 1.9,
        name: 'Click Income'
    },
    multiplier: {
        start: 200,
        inc: 2.3,
        name: 'Income Multiplier'
    }
}

export default class App extends React.Component {
    constructor() {
        super();
        this.state = {
            timer: 0,
            totalScore: 0,
            score: 0,
            upgrades: {
                auto: 0,
                click: 0,
                multiplier: 0,
            }
        }
    }

    render() {
        const income = this.state.upgrades.auto
            ? (
                <div className="income">
                    Income: {Math.round(this.state.upgrades.auto * Math.pow(1.25, this.state.upgrades.multiplier))}/s
                </div>
            )
            : null

        return (
            <div style={{"margin": "20px"}}>
                <div className="score">
                    Score: {Math.floor(this.state.score)}
                </div>
                <div className="score">
                    Total Score: {Math.floor(this.state.totalScore)}
                </div>
                {income}

                <div style={{'margin-bottom': '50px'}}>
                    <div className="button" onClick={() => this.handleClick()}>
                        Click for score!
                    </div>
                </div>

                <Upgrades
                    upgradesHandler={params => this.handleUpgrade(params)}
                    upgrades={this.state.upgrades}
                />
            </div>
        )
    }

    handleClick() {
        const ipc = Math.round((this.state.upgrades.click + 1) * Math.pow(1.25, this.state.upgrades.multiplier));
        this.setState({
            score: this.state.score + ipc,
            totalScore: this.state.totalScore + ipc,
        })
    }

    handleUpgrade(upgrade) {
        const cost = Math.ceil(UPGRADES_DATA[upgrade].start * (Math.pow(UPGRADES_DATA[upgrade].inc, this.state.upgrades[upgrade])));
        if (cost <= this.state.score) {
            const up = this.state.upgrades;
            up[upgrade]++;
            this.setState({
                score: this.state.score - cost,
                upgrade: up,
            })
        }
    }

    componentDidMount() {
        window.requestAnimationFrame(this.tick.bind(this))
    }

    tick(now) {
        window.requestAnimationFrame(this.tick.bind(this))
        const time = now - this.state.timer,
            speed = time / 1000,
            income = this.state.upgrades.auto * Math.pow(1.25, this.state.upgrades.multiplier),
            score = this.state.score + income * speed,
            totalScore = this.state.totalScore + income * speed;

        this.setState({timer: now, score, totalScore})
    }
}